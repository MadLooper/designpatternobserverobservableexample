import java.util.LinkedList;
import java.util.List;
import java.util.Observable;

/**
 * Created by KW on 8/10/17.
 */
public class McDonald extends Observable{

    private List<Employee> employeeList = new LinkedList<>();
    public McDonald() {
        for(int i=0; i < 10; i++){
            Employee e = new Employee();
            employeeList.add(e);        //generujemy observerów

            //
            addObserver(e); // dodajemy observera
//            deleteObserver(e); // jeśli chcemy, możemy usunąć observera
        }
    }

    public void handleOrder(String order){
        setChanged();               // zaznaczamy zmianę
        notifyObservers(order);     // informujemy o zmianie i wiadomości
                                // którą chcemy przekazać do observerów
    }
}
